import AddNewImageboardForm from "./components/AddNewImageboardForm/AddNewImageboardForm";
import Layout from "./components/UI/Layout/Layout";
import {useEffect} from "react";
import {Box, Grid, Modal, Stack, Typography} from "@mui/material";
import {useDispatch, useSelector} from "react-redux";
import {fetchImageboards, openImageboardModal, postImageboard} from "./store/actions/contactsActions";
import BackdropPreloader from "./components/UI/BackdropPreloader/BackdropPreloader";
import Imageboard from "./components/Imageboard/Imageboard";
import {LoadingButton} from "@mui/lab";

const style = {
   position: 'absolute',
   top: '50%',
   left: '50%',
   transform: 'translate(-50%, -50%)',
   width: 400,
   bgcolor: 'background.paper',
   border: '2px solid #000',
   boxShadow: 24,
   p: 4,
};

const App = () => {
   const dispatch = useDispatch();
   const imageboards = useSelector(state => state.imageboards.data);
   const loading = useSelector(state => state.imageboards.loading);
   const postBtnLoading = useSelector(state => state.imageboards.postBtnLoading);
   const showImageboardModal = useSelector(state => state.imageboards.showImageboardModal);

   useEffect(() => {
      dispatch(fetchImageboards());
   }, [dispatch]);

   return (
     <Layout>
        <>
           {loading && (<BackdropPreloader option={{type: 2}}/>)}
           {imageboards && (
             <Stack>
                <LoadingButton
                  loading={postBtnLoading}
                  onClick={() => dispatch(openImageboardModal(true))}
                >
                   Add
                </LoadingButton>
                <Modal
                  open={showImageboardModal}
                  onClose={() => dispatch(openImageboardModal(false))}
                  aria-labelledby="modal-modal-title"
                  aria-describedby="modal-modal-description"
                >
                   <Box sx={style}>
                      <Stack spacing={2}>
                         <Typography variant={'h6'} textAlign={"center"}>Fill in the input fields to create an
                            imageboard.</Typography>
                         <AddNewImageboardForm
                           onSubmitHandler={(formData) => dispatch(postImageboard(formData))}
                         />
                      </Stack>
                   </Box>
                </Modal>
                <Grid container spacing={2}>
                   {imageboards.map((obj) => {
                      return (
                        <Imageboard
                          key={obj.id}
                          author={obj.author}
                          description={obj.description}
                          image={obj.image}
                        />
                      )
                   })}
                </Grid>
             </Stack>
           )}
        </>
     </Layout>
   );
};

export default App;
