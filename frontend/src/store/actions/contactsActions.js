import {IMAGEBOARDS_URL} from "../../config";
import {axiosApi} from "../../axiosApi";

export const OPEN_IMAGEBOARD_MODAL = 'OPEN_IMAGEBOARD_MODAL';
export const FETCH_IMAGEBOARDS_REQUEST = 'FETCH_IMAGEBOARDS_REQUEST';
export const FETCH_IMAGEBOARDS_SUCCESS = 'FETCH_IMAGEBOARDS_SUCCESS';
export const FETCH_IMAGEBOARDS_FAILURE = 'FETCH_IMAGEBOARDS_FAILURE';
export const POST_IMAGEBOARD_REQUEST = 'POST_IMAGEBOARD_REQUEST';
export const POST_IMAGEBOARD_SUCCESS = 'POST_IMAGEBOARD_SUCCESS';
export const POST_IMAGEBOARD_FAILURE = 'POST_IMAGEBOARD_FAILURE';

export const openImageboardModal = (bool) => ({type: OPEN_IMAGEBOARD_MODAL, payload: bool});

export const fetchImageboardsRequest = () => ({type: FETCH_IMAGEBOARDS_REQUEST});
export const fetchImageboardsSuccess = (data) => ({type: FETCH_IMAGEBOARDS_SUCCESS, payload: data});
export const fetchImageboardsFailure = (error) => ({type: FETCH_IMAGEBOARDS_FAILURE, payload: error});

export const fetchImageboards = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchImageboardsRequest());
      const {data} = await axiosApi.get(IMAGEBOARDS_URL);
      dispatch(fetchImageboardsSuccess(data));
    } catch (error) {
      dispatch(fetchImageboardsFailure(error));
    }
  }
};

export const postImageboardRequest = () => ({type: POST_IMAGEBOARD_REQUEST});
export const postImageboardSuccess = () => ({type: POST_IMAGEBOARD_SUCCESS});
export const postImageboardFailure = (error) => ({type: POST_IMAGEBOARD_FAILURE, payload: error});

export const postImageboard = (formData) => {
  return async (dispatch) => {
    try {
      dispatch(postImageboardRequest());
      await axiosApi.post(IMAGEBOARDS_URL, formData);
      dispatch(fetchImageboards());
      dispatch(postImageboardSuccess());
    } catch (error) {
      dispatch(postImageboardFailure(error));
    }
  }
};
