import {
   FETCH_IMAGEBOARDS_FAILURE,
   FETCH_IMAGEBOARDS_REQUEST,
   FETCH_IMAGEBOARDS_SUCCESS,
   OPEN_IMAGEBOARD_MODAL,
   POST_IMAGEBOARD_FAILURE,
   POST_IMAGEBOARD_REQUEST,
   POST_IMAGEBOARD_SUCCESS
} from "../actions/contactsActions";

const initState = {
   loading: null,
   postBtnLoading: false,
   showImageboardModal: false,
   data: null,
   error: null,
};
export const imageboardsReducers = (state = initState, action) => {
  switch (action.type) {
     case OPEN_IMAGEBOARD_MODAL:
        return {...state, showImageboardModal: action.payload}
     case FETCH_IMAGEBOARDS_REQUEST:
        return {...state, loading: true, error: null}
     case FETCH_IMAGEBOARDS_SUCCESS:
        return {...state, loading: false, data: action.payload}
     case FETCH_IMAGEBOARDS_FAILURE:
        return {...state, loading: false, error: action.payload}
     case POST_IMAGEBOARD_REQUEST:
        return {...state, postBtnLoading: true, error: null}
     case POST_IMAGEBOARD_SUCCESS:
        return {...state, postBtnLoading: false, showImageboardModal: false}
     case POST_IMAGEBOARD_FAILURE:
        return {...state, postBtnLoading: false, error: action.payload}
    default:
      return state;
  }
}