import React from 'react';
import {AppBar, Toolbar, Typography} from "@mui/material";

const AppToolbar = () => {
   return (
     <>
        <AppBar position={"fixed"}>
           <Toolbar>
              <Typography variant={"h6"}>
                 Imageboards
              </Typography>
           </Toolbar>
        </AppBar>
        <Toolbar/>
     </>
   );
};

export default AppToolbar;