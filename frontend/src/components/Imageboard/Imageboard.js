import React from 'react';
import {BASE_URL} from "../../config";
import {Card, CardContent, CardHeader, CardMedia, Grid, Typography} from "@mui/material";
import {makeStyles} from "@mui/styles";

const useStyles = makeStyles({
   card: {
      height: '100%'
   },
   media: {
      height: 0,
      paddingTop: '56.25%'
   }
});

const Imageboard = ({description, author, image}) => {
   const classes = useStyles();

   let cardImage = null;

   if (image) {
      cardImage = BASE_URL + '/uploads/' + image;
   }

   return (
     <Grid item xs={12} sm={6} md={6} lg={4}>
        <Card className={classes.card}>
           <CardHeader title={author}/>
           {cardImage && (
             <CardMedia
               image={cardImage}
               title={author}
               className={classes.media}
             />
           )}
           <CardContent>
              <Typography variant="subtitle1">
                 {description}
              </Typography>
           </CardContent>
        </Card>
     </Grid>
   );
};

export default Imageboard;