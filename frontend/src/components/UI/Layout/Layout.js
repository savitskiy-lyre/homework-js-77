import React from 'react';
import {Container, CssBaseline} from "@mui/material";
import AppToolbar from "../../../containers/AppToolbar/AppToolbar";
import './Layout.css';

const Layout = ({children}) => {
   return (
     <div className={'content-height'}>
        <CssBaseline/>
        <AppToolbar/>
        <main className={'bg-color content-grow'}>
           <Container>
              {children}
           </Container>
        </main>
     </div>
   );
};

export default Layout;