import React, {useState} from 'react';
import {Grid, TextField} from "@mui/material";
import {LoadingButton} from "@mui/lab";

const AddNewImageboardForm = ({onSubmitHandler}) => {
   const [state, setState] = useState({
      author: '',
      description: '',
      image: null,
   })

   const inputChangeHandler = (e) => {
setState((prevState) => ({...prevState, [e.target.name]: e.target.value}));
   };

   const fileChangeHandler = e => {
      setState(prevState => ({...prevState, [e.target.name]: e.target.files[0]})
      );
   };

  const onSubmit = (e) => {
     e.preventDefault();
     const formData = new FormData();
     Object.keys(state).forEach((key) => {
        formData.append(key, state[key]);
     })
     onSubmitHandler(formData);
  };

  return (
    <form onSubmit={onSubmit}>
      <Grid container direction={'column'} spacing={2}>
        <Grid item>
          <TextField
            fullWidth
            label="Author"
            name={'author'}
            value={state.author}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item>
          <TextField
            required
            fullWidth
            label="Message"
            name={'description'}
            value={state.description}
            onChange={inputChangeHandler}
          />
        </Grid>
        <Grid item>
           <input
             type="file"
             name={'image'}
             onChange={fileChangeHandler}
           />
        </Grid>
        <Grid item textAlign={"center"}>
          <LoadingButton
            loading={false}
            type={"submit"}
            variant={"contained"}
            color={"success"}
            size={'large'}
          >
            Add
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default AddNewImageboardForm;