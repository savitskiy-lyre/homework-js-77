const express = require("express");
const cors = require('cors');
const fileDb = require('./DB/db');
const encode = require('./sections/encode');
const decode = require('./sections/decode');
const imageboards = require('./sections/imageboards');
const messages = require('./sections/messages');
const {ENCODE_PATH, DECODE_PATH, MESSAGES_PATH, IMAGEBOARS_PATH} = require("./constants");

const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8080;

app.use(ENCODE_PATH, encode);
app.use(DECODE_PATH, decode);
app.use(MESSAGES_PATH, messages);
app.use(IMAGEBOARS_PATH, imageboards);

app.get('/:url', (req, res) => {
   res.send(req.params.url);
})
app.get('/', (req, res) => {
   res.send('Hello');
})
fileDb.init();
app.listen(port, () => {
   console.log('We are live ~~!!! on port ' + port);
})