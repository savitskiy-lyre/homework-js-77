const ENCODE_PATH = '/encode';
const DECODE_PATH = '/decode';
const MESSAGES_PATH = '/messages';
const IMAGEBOARS_PATH = '/imageboards';

module.exports = {
   ENCODE_PATH,
   DECODE_PATH,
   MESSAGES_PATH,
   IMAGEBOARS_PATH,
}