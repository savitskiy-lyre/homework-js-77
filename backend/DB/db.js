const fs = require('fs');
const {nanoid} = require('nanoid');
const {IMAGEBOARS_PATH} = require("../constants");

const filename = './DB/database.json';
let data = [];

module.exports = {
   init() {
      try {
         const fileContents = fs.readFileSync(filename);
         data = JSON.parse(fileContents);
      } catch (e) {
         data = [];
         fs.writeFileSync(filename, JSON.stringify(data));
      }
   },
   getItems(date, str) {
      if (str === IMAGEBOARS_PATH) return data.filter((currentObj) => currentObj.description);
      if (date) return data.filter((currentObj) => new Date(currentObj.datetime) - new Date(date) > 0);
      return data;
   },
   getItem(id) {
      return data.find(i => i.id === id);
   },
   addItem(item) {
      item.id = nanoid();
      item.datetime = new Date().toISOString();
      data.push(item);
      this.refresh();
      this.save();
      return item;
   },
   save() {
      fs.writeFileSync(filename, JSON.stringify(data));
   },
   refresh() {
      data.sort((a, b) => {
         return new Date(a.datetime) - new Date(b.datetime);
      })
   }
};