const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const fileDb = require('../DB/db');
const {IMAGEBOARS_PATH} = require("../constants");

const storage = multer.diskStorage({
   destination: (req, file, cb) => {
      cb(null, config.uploadPath);
   },
   filename: (req, file, cb) => {
      cb(null, nanoid() + path.extname(file.originalname));
   }
});

const upload = multer({storage});

const section = express.Router();
section.get('/', (req, res) => {
   const imageboard = fileDb.getItems(null, IMAGEBOARS_PATH);
   res.send(imageboard);
});

section.get('/:id', (req, res) => {
   const imageboard = fileDb.getItem(req.params.id);
   if (!imageboard) {
      return res.status(404).send({error: 'Imageboard not found'});
   }

   res.send(imageboard);
});

section.post('/', upload.single('image'), (req, res) => {
   if (!req.body.description) {
      return res.status(400).send({error: 'Data not valid'});
   }

   const imageboard = {
      author: req.body.author ? req.body.author : 'Anonymous',
      description: req.body.description,
   };

   if (req.file) {
      imageboard.image = req.file.filename;
   }

   const newProduct = fileDb.addItem(imageboard);

   res.send(newProduct);
});

module.exports = section;