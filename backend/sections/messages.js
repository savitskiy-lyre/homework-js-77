const express = require("express");
const section = express.Router();
const fileDb = require('../DB/db');

section.get('/', (req, res) => {
   let itemsFromDb;
   if (req.query.datetime) {
      if (isNaN(new Date(req.query.datetime).getDate())) {
         console.log(true);
         return res.status(400).send({error: 'Wrong date'});
      }
      itemsFromDb = fileDb.getItems(req.query.datetime);
   } else {
      itemsFromDb = fileDb.getItems();
   }
   res.send(itemsFromDb);
})

section.post('/', (req, res) => {
   if (!req.body.message || !req.body.author) {
      let error = {};
      if (!req.body.message) error.message = 'Not found';
      if (!req.body.author) error.author = 'Not found';
      return res.status(400).send(error);
   }
   const newItem = fileDb.addItem({
      author: req.body.author,
      message: req.body.message,
   });
   //req.body.query = req.query;
   res.send(newItem);
})

module.exports = section;