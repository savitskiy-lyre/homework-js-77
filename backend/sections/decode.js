const express = require("express");
const Vigenere = require('caesar-salad').Vigenere;
const section = express.Router();


section.get('/:data', (req, res) => {
   res.send(Vigenere.Decipher('data').crypt(req.params.data));
})
section.post('/', (req, res) => {
   if (!req.body.password) res.status(404).send("Wrong data");
   res.send({message: Vigenere.Decipher(req.body.password).crypt(req.body.message)});
})

module.exports = section;